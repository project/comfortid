
// ---
// Global variables:
// 	comfortidModulePath - module path. useful to access module resources like images 
// ---

Drupal.behaviors.comfortid = function (context) {
	// The image button which shows the provider list 
	var icon = $('#comfortid-openid-selector > .openid-icon');
	
	// All providers container
	var openid;
	
	// The provider from the list of available ones
	var selectedProvider;
	
	// Catch mousedown event for the document in order to hide the popup
	$(document).bind('mousedown', function(e){
			var el = e.target;
			var list = $('#openid-provider-list');
			while (list){
				if (el == list[0] || el == icon[0]) {
					return false;
				} else if (el == document) {
					list.fadeOut('fast');
					return true;
				} else {
					el = $(el).parent()[0];
				}
			}
		
	});
	
	$('#edit-comfortid-openid-identifier:not(.comfortid-processed)', context)
		.addClass('comfortid-processed')
		// Let the user know which provider is in use
		.focus(function () {
			icon
				.fadeOut('fast')
				.fadeIn('fast');
			
			retrieveProviders($('#comfortid-openid-selector')[0].href, false);
		})
		// Check which scheme is in use and select the 
		.keyup(function(e){
			if(icon.hasClass('comfortid-loaded')){
				// Try to preselect according to the shortcut, in case the user has entered it
				selectProviderByShortcut(this.value);
			}
		})
		.blur(function(){
			// Process user value into a correct OpenID
			var _processUserValue = function (){
				if(selectedProvider){
					var pattern = new RegExp("^.*:[/]*");
					if(pattern($('#edit-comfortid-openid-identifier').val())){
						// Shortcut was used, remove it
						var userValue = $('#edit-comfortid-openid-identifier').val().replace(/^.*:[/]*/, "");
					}else{
						// Shortcut was not used
						userValue = $('#edit-comfortid-openid-identifier').val();
					}
					$('#edit-comfortid-openid-hidden-identifier').val(selectedProvider["url-pattern"].replace(/\[id\]/, userValue));
				}else{
					// Use raw user value
					$('#edit-comfortid-openid-hidden-identifier').val($('#edit-comfortid-openid-identifier').val());
				}
			}
			
			if(selectedProvider){
				_processUserValue();
			}else{
				// Try to select the provider
				selectProviderByShortcut($('#edit-comfortid-openid-identifier').val());
				_processUserValue();
			}
		});
  
  // Generate the provider popup and show/hide it when necessary
  $('#comfortid-openid-selector:not(.comfortid-processed)', context)
  	.addClass('comfortid-processed')
  	.click(function () {
  		this.blur();
  		retrieveProviders(this.href, true);
			return false;
  	});
  	
  $('#comfortid-login-form:not(.comfortid-processed)', context)
  	.addClass('comfortid-processed')
  	.submit(function() {
  		// TODO: Handle the scenario:
  		//
  		// 1. User enters the id
  		// 2. Presses [Sign in]
  		// 3. Gets to the provider page
  		// 4. Presses browser [Back] button
  		// 5. The user id is still in the textfield (e.g. FF3), but providers are not loaded
  		// 6. Presses [Sign in]. FAILURE, because the provider was not (pre)selected.
  	});
  
  /*
  	Determine the shortcut and try to select the provider with that shortcut
  */
  function selectProviderByShortcut(userValue){
 		var pattern = new RegExp("^.+:[/]*");
		var shortcut = pattern.exec(userValue);
		if(shortcut && openid){
			shortcut = new String(shortcut);
			shortcut = shortcut.substring(0, shortcut.indexOf(':'));
			shortcut = $.trim(shortcut);
			
			for(var i = 0; i < openid.providers.length; i++){
				var p = openid.providers[i];
				if(p.shortcut == shortcut){
					selectedProvider = p; // SELECTED
					icon.attr('src', selectedProvider.icon);
					break;
				}
			}
		}
  }
  
 	/*
 		Obtains the OpenID provider list. Does nothing if it's already downloaded.
 	*/
 	function retrieveProviders(url, showList){
 	
 		var oldIconSrc = icon.attr('src');
 		var providerContainer = $('#openid-provider-list');
 		
 		if(!icon.hasClass('comfortid-loaded') && !icon.hasClass('comfortid-loading')) {
 			icon.addClass('comfortid-loading');
  		icon.attr('src', comfortidModulePath+'/img/loading.gif');
  		providerContainer = $('#comfortid-login-form > div').append('<table id="openid-provider-list"></table>').find('#openid-provider-list');
 		} else if(icon.hasClass('comfortid-loading')) {
 			return;
 		}
 		
 		providerContainer.hide();
 		
 		var _show = function(){
			if(showList){
				providerContainer.fadeIn('slow');
				var targetPosLeft = $('#comfortid-openid-selector').position().left - providerContainer.find('tr:first').find('td:first').outerWidth();
				providerContainer.css('left', targetPosLeft+'px');
			}
			// In case there is a user value
			selectProviderByShortcut($('#edit-comfortid-openid-identifier').val());
 		}
 		
 		if(icon.hasClass('comfortid-loading')) {
 			var _providers = function (data) {
				openid = data;
				
				for(var i = 0; i < openid.providers.length; i++){
					var currentProvider = openid.providers[i];
					var index = new Number(i.valueOf());
					providerContainer.append(
						'<tr class="openid-provider-list-item-'+(i % 2 == 0 ? 'even' : 'odd')+'">'+
							'<td class="openid-provider-shortcut">'+currentProvider.shortcut+':</td>'+
							'<td class="openid-provider-icon"><img src="'+currentProvider.icon+'" class="openid-icon"/></td>'+
							'<td class="openid-provider-name">'+currentProvider.name+'</td>'+
						'</tr>');
				}
				
				//TODO: Load from a cookie
				
				 
				providerContainer.find('tr').click(function(){
					var index = providerContainer.find('tr').index(this);
					selectedProvider = openid.providers[index]; // SELECTED
					icon.attr('src', selectedProvider.icon);
					providerContainer.fadeOut('fast');
					
					var userValue = $('#edit-comfortid-openid-identifier').val();
					userValue = userValue.replace(/^.*:[/]*/, "");
					$('#edit-comfortid-openid-identifier').val(userValue) 
					$('#edit-comfortid-openid-identifier').focus();
					
					//TODO: Save to a cookie
					
				});
				
				icon.attr('src', oldIconSrc);
				icon.addClass('comfortid-loaded');
				icon.removeClass('comfortid-loading');
				
				_show();
			}
			
			//TODO: handle both onsuccess and onfailure
			$.getJSON(url, _providers);
 		}else if(icon.hasClass('comfortid-loaded')){
 			_show();
 		}
 	}
};